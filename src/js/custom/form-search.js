	/*----------------------------------------
	 SEARCH IN HEADER
	 ----------------------------------------*/
	var buttonSearch = $('#search-open'),
		buttonCloseSearch = $('#search-close'),
		searchForm = $('#search'),
		searchInput = searchForm.find('input[type="search"]');



	function searchOpen(){
		searchForm.toggleClass('active');
		setTimeout(function () {
			searchInput.focus();
			buttonSearch.attr('type', 'submit');
		}, 400);
	}
	function searchClose(){
		searchForm.removeClass('active');
		buttonSearch.attr('type', 'button');
	}

	buttonSearch.on('click', function () {
		searchOpen();
	});

	buttonCloseSearch.on('click', function () {
		searchClose();
	});


	var clickEvent = ((document.ontouchstart!==null)?'click':'touchstart');

	$(document).on(clickEvent, function(e) {
		if (!$(e.target).closest(searchForm).length) {
			searchClose();
		}
		e.stopPropagation();
	});