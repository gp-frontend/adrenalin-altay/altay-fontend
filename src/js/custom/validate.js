	/*----------------------------------------
	 VALIDATIONS JQUERY FORM
	 ----------------------------------------*/
	$(function () {
		/* RULES */
		$("#form-contacts").validate({
			focusCleanup: true,
			focusInvalid: false,
			errorElement: "span",
			submitHandler: function(element) {
				// $('#popup-thanks').gpPopup();
			}
		});
		$("#form-questions").validate({
			focusCleanup: true,
			focusInvalid: false,
			errorElement: "span",
			submitHandler: function(element) {
				// $('#popup-thanks').gpPopup();
			}
		});
	});
