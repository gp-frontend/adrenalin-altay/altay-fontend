	/*----------------------------------------
		MENU
	----------------------------------------*/

	var ESC_KEYCODE = 27;
	var ENTER_KEYCODE = 13;

	var clickEvent = ((document.ontouchstart!==null)?'click':'touchstart'),
			menuIsOpen = false;

	var menu = $('#menu'),
			buttonMenu = $('#button-menu'),
			buttonFooter = $('#footer-menu'),
			btnCloseMenu = menu.find('.js-close'),
			toggleMenu = $('.menu__toggle');



	//OPEN MENU
	function openMenu() {
		menu.addClass('menu_open');
		$('body').addClass('menu-open');
	}
	//CLOSE MENU
	function closeMenu(someMenu) {
		if(someMenu){
			someMenu.removeClass('menu_open');
		}else{
			menu.removeClass('menu_open');
		}
		$('body').removeClass('menu-open');
	}

	//OPEN DROPDOWN
	function openDropdown() {
		$(this)
		.toggleClass('clicked')
		.parent('li')
		.toggleClass('active')
		.siblings('li')
		.removeClass('active')
		.find('.submenu')
		.slideUp();

		$(this)
		.siblings('.submenu')
		.slideToggle();
	}

	function createPanelSwipe() {
		var panelSwipe = $('<div />').attr('id','panelswipe');
		panelSwipe.appendTo('body');
	}

	// OPEN EVENTS
	buttonMenu.click(function(e) {
		e.preventDefault();
		openMenu();
	});
	buttonFooter.click(function(e) {
		e.preventDefault();
		openMenu();
	});

	toggleMenu.on('click', openDropdown) ;


	// CLOSE EVENTS
	btnCloseMenu.click(function(e) {
		e.preventDefault();
		closeMenu();
	});

	createPanelSwipe();

	// SWIPE EVENTS

	var panelMenu = $('#panelswipe');

	panelMenu.swipe({
		swipeRight: function () {
			openMenu();
		}
	});

	menu.swipe({
		swipeLeft: function () {
			closeMenu();
		}
	});

	/* PRESS ESC BUTTON */
	$(document).keydown(function(evt) {
		if( evt.keyCode === ESC_KEYCODE ) {
			closeMenu();
			return false;
		}
	});
