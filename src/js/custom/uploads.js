/*----------------------------------------
UPLOADS FILE
----------------------------------------*/
$(".uploads__input").on('change', function() {
	//Get count of selected files
	var countFiles = $(this)[0].files.length;
	var imgPath = $(this)[0].value;
	var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

	var idLabel = $(this).attr('data-label'),
			label = $('#' + idLabel);

	var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');


	if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		//change label text
		$(label).text(file_name);
	}

});
