	/*----------------------------------------
	 FOR SVG SPRITE
	 ----------------------------------------*/
	svg4everybody({});

	/*----------------------------------------
	 TRANSITION SCROLL
	 ----------------------------------------*/
	$('.scroll').on("click", function(e) {
		// e.preventDefault();
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 1000);
	});

	/*----------------------------------------
		PHONE MASK
	----------------------------------------*/
	$(".mask-number").mask("0#");
	$(".mask-phone").mask("+7 (000) 000-00-00");
	$(".mask-time").mask("00 : 00");
	$(".mask-date").mask("00.00.0000");


	/*----------------------------------------
		PHONE BUG
	----------------------------------------*/
	$(document).ready(function() {
		// Detect ios 11_0_x affected
		// NEED TO BE UPDATED if new versions are affected
		var ua = navigator.userAgent,
		iOS = /iPad|iPhone|iPod/.test(ua),
		iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);

		// ios 11 bug caret position
		if ( iOS && iOS11 ) {

		// Add CSS class to body
		$("body").addClass("iosBugFixCaret");

		}

	});

	/*----------------------------------------
		INIT SLICK SLIDER
	----------------------------------------*/
	$('.js-slider').slick({
		fade: true,
		cssEase: 'linear',
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		swipe: false,
		speed: 600,
		prevArrow: '',
		nextArrow: $('.first-screen-title__button')
	});

	$('.js-weather').slick({
		cssEase: 'linear',
		infinite: true,
		slidesToShow: 8,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		responsive: [
		{
			breakpoint: 980,
			settings: {
				slidesToShow: 6
			 }
		 },
	 ]
	});

	// slider
		var $gallery = $('.js-gallery'),
				$items = $('.js-alloy-items'),
				$article = $('.js-article');

		settings_slider = {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots:true,
			arrows: false,
			autoplay: false,
			autoplaySpeed: 2000,
			mobileFirst: true,
		}

	// slick on mobile
		function slick_on_mobile(slider, settings){
			$(window).on('load resize', function() {
				if ($(window).width() > 767) {
					if (slider.hasClass('slick-initialized')) {
						slider.slick('unslick');
					}
					return
				}
				if (!slider.hasClass('slick-initialized')) {
					return slider.slick(settings);
				}
			});
		};


		slick_on_mobile( $gallery, settings_slider);
		slick_on_mobile( $items, settings_slider);
		slick_on_mobile( $article, settings_slider);


	/*----------------------------------------
		FILTER DATE
	----------------------------------------*/

	var dateFilter = $('.date-filter-list'),
	dateFilterItem = $('.date-filter-list__item'),
	dateButton = $('.date-filter__button');

	function dateFilterInit() {
		dateFilter.each(function() {
			$(this)
			.find('.date-filter-list__item')
			.first()
			.addClass('date-filter-list__item_active')
			.siblings()
			.removeClass('date-filter-list__item_active');
		});
	}
	function dateButtonInit() {
		var text = $('.date-filter-list__item_active').text();
		dateButton.text(text);
	};

	function dateFilterActivate() {
		$(this)
		.addClass('date-filter-list__item_active')
		.siblings()
		.removeClass('date-filter-list__item_active');
		dateButtonInit();
		dateFilterClose();
	}

	function dateButtonClick() {
		$(this).toggleClass('clicked').siblings('.date-filter-list').toggleClass('open');
	}

	function dateFilterClose() {
		dateButton.removeClass('clicked');
		$('.date-filter-list').removeClass('open');
	}

	dateFilterInit();
	dateButtonInit();

	dateFilterItem.on('click', dateFilterActivate);
	dateButton.on('click', dateButtonClick);

		var clickEvent = ((document.ontouchstart!==null)?'click':'touchstart');

		/* CLOSE ON CLICK ON DOCUMENT */
		$(document).on(clickEvent, function(e) {
			if (!$(e.target).closest('.date-filter').length) {
				dateFilterClose();
			}
			e.stopPropagation();
		});

	/*----------------------------------------
	SUMO SELECT && DATEPICKER
	----------------------------------------*/
	$(document).ready(function () {
		//select
		$('.select').SumoSelect();
		// //datepicker
		var $date = $('[data-toggle="datepicker"]');

		$date.datepicker({
			format: 'dd.mm.yyyy',
			language: 'ru-RU'
		});

	});
	/*----------------------------------------
			OPTIONS FANCYBOX
	----------------------------------------*/
	$("[data-fancybox]").fancybox({
		buttons : [
				// 'slideShow',
				// 'fullScreen',
				'thumbs',
				// 'share',
				//'download',
				//'zoom',
				'close'
		],
	});
